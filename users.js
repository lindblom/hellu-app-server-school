var users = [
    { id: 1, username: 'Christopher', password: 'pass' },
    { id: 2, username: 'Annelie',     password: 'pass' },
    { id: 3, username: 'Lindblom',    password: 'pass' }
];

var nextId = 4;

function find(id) {
    for (var i = 0; i < users.length; i++) {
        if (users[i].id === id)
            return users[i];
    }
}

function findUserByUsername(username) {
    for (var i = 0; i < users.length; i++) {
        if (users[i].username.toUpperCase() === username.toUpperCase())
            return users[i];
    }
}

function isValidUserForm(userForm) {

    if(typeof userForm.username != 'string' && typeof userForm.password != 'string')
        return false;

    if(userForm.username.length == 0 || userForm.password.length == 0)
        return false;

    return true;
}

function signup(userForm) {
    if(findUserByUsername(userForm.username))
        return null;

    var newUser = {
        id: nextId++,
        username: userForm.username,
        password: userForm.password
    };

    users.push(newUser);

    return newUser;
}

function filterUser(user) {
    return {
        id: user.id,
        username: user.username
    }
}

exports = module.exports = {
    getUser: function (username) {
        return findUserByUsername(username);
    },
    isValidUserForm: isValidUserForm,
    signup: signup,
    getUsers: function() {
        return users.map(function (user) { return filterUser(user) });
    },
    filterUser: filterUser,
    find: find
}
