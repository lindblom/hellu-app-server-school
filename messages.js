var users = require('./users');

var messages = [
        {
            userId: 2,
            senderId: 1,
            message: "Hej detta är ett meddelande!"
        },
        {
            userId: 1,
            senderId: 2,
            message: "Och detta är ett svar..."
        },
        {
            userId: 3,
            senderId: 2,
            message: "Lindblom skriver detta till Annelie!"
        }
    ];

function getMessages(userId1, userId2) {
    return messages.filter(function (e) {
        return (e.userId == userId1 || e.senderId == userId1) &&
               (e.userId == userId2 || e.senderId == userId2)
    }).map(function (e) {
        e.sender = users.filterUser(users.find(e.userId))
        return e;
    });
}

function sendMessage(senderId, userId, message) {
    var newMessage = {
        userId: userId,
        senderId: senderId,
        message: message
    };

    messages.push(newMessage);

    return newMessage;
}

exports = module.exports = {
    getMessages: getMessages,
    sendMessage: sendMessage
}
