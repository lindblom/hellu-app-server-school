var users = require('./users');

var TOKEN_NAME = 'hellulogintoken';

var openSessions = {
    TOKEN1: 1,
    TOKEN2: 2
};

function createSessionForUser(user) {
    var sessionKey = createSessionKey();
    openSessions[sessionKey] = user.id;
    return sessionKey;
}

function destroySession(sessionKey) {
    delete openSessions[sessionKey];
}

function createSessionKey() {
    var avaliableCharacters = "QWERTYUIOPASDFGHJKLZXCVBNM",
        sessionKey = "";
        randomNum = createRandom(0, avaliableCharacters.length - 1);

    while (sessionKey.length < 20) {
        sessionKey = sessionKey.concat(avaliableCharacters[randomNum()]);
    }

    return sessionKey;
}

function createRandom(min, max) {
    return function () {
        return Math.floor(Math.random() * (max - min + 1)) + min;;
    }
}

function authenticateUser(req, res, next) {
    if(req.get(TOKEN_NAME))
        req.user = users.find(openSessions[req.get(TOKEN_NAME)]);

    next();
}

function requireAuthentication(req, res, next) {
    if(!req.user)
        res.send(401);
    else
        next();
}

exports = module.exports = {
    createSessionForUser: createSessionForUser,
    destroySession: destroySession,
    authenticateUser: authenticateUser,
    requireAuthentication: requireAuthentication,
    TOKEN_NAME: TOKEN_NAME
}
