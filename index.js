var express = require('express')
    app = express(),
    users = require('./users'),
    sessions = require('./sessions'),
    messages = require('./messages');

app.use(express.bodyParser());

app.use(sessions.authenticateUser);

app.post('/login', function (req, res) {
    var user = users.getUser(req.body.username);

    if (user && user.password === req.body.password) {
        user = users.filterUser(user);
        user.sessionKey = sessions.createSessionForUser(user);
        res.send(user);
    } else {
        res.send(402);
    }
});

app.post('/signup', function (req, res) {
    var userForm = req.body;

    if (users.isValidUserForm(userForm)) {
        var newUser = users.signup(userForm);

        if (newUser) {
            newUser = users.filterUser(newUser);
            newUser.sessionKey = sessions.createSessionForUser(newUser);
            res.send(201, newUser);
        }
        else
            res.send(422, "Användarnamnet var redan upptaget.");
    } else
        res.send(400);
});

app.get('/users', sessions.requireAuthentication, function (req, res) {
    res.send(users.getUsers().filter(function (e) {
        return e.id != req.user.id;
    }));
});

app.get('/messages/:userId', sessions.requireAuthentication, function (req, res) {
    res.send(messages.getMessages(+req.user.id, +req.params.userId))
});

app.post('/messages/:userId', sessions.requireAuthentication, function (req, res) {
    var message = req.body.message;
    res.send(messages.sendMessage(+req.params.userId, +req.user.id, message));
});

app.del('/logout', sessions.requireAuthentication, function (req, res) {
    sessions.destroySession(req.get(sessions.TOKEN_NAME));
    res.send(200);
});

app.listen(3000, function() {
    console.log('Listingen on port 3000');
});
